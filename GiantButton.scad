module halfSphere(radius)
{
    r = radius;

    difference()
    {
        sphere(r);

        translate([0, 0, -r])
            cube([r*2, r*2, r*2], center=true);
    }
}

module dome(radius, thickness)
{
    difference()
    {
        halfSphere(radius);
        halfSphere(radius-thickness);
    }

}

module helix(radius, width, height)
{
    r = radius;
    w = width;
    h = height;

    linear_extrude(height = h, twist = 360)
    {
        translate([r-w*2, 0 ,0])
            square([10,10]);
    }
}


module multiHelix(radius, width, height, n )
{
    for (i = [0:n-1])
    {
        rotate([0, 0, 360/n*i])
            helix(radius, width, height);
    }
}


module base(radius, height)
{
    r = radius;
    h = height;

    cylinder(h, r, r);
}


//Settings:
$fn=100;
DomeRadius = 50;
DomeThickness = 2;
HelixWidth = 3;
BaseRadius = DomeRadius - DomeThickness *4;
BaseHeight = 10;


//Main:
translate([0, 0, -BaseHeight]) 
    base(BaseRadius, BaseHeight);

dome(DomeRadius, DomeThickness);

intersection()
{
    multiHelix(BaseRadius*0.9, HelixWidth, DomeRadius, 3);
    halfSphere(DomeRadius);
}